<?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?><!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >

		  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
		  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

		<link rel="profile" href="https://gmpg.org/xfn/11">

<style>
#site-header{
	background: #586f5f !important;
}

body{
	background: #ffffff !important;
}

.header-toggles:only-child .toggle-inner .toggle-text, .header-toggles:only-child .toggle-inner .svg-icon{
	color: #ffffff !important;
}

.logo21 {
    text-align: center !important;
    display: block;
    width: 400px;
    margin: 0 auto;
    padding-top: 30px;
    padding-bottom: 30px;
}

.banner01 img{
	height: auto;
	width: 100%;
}

.banner01 .info{
    position: absolute;
    margin-top: -70px;
    padding-left: 30px;
}

.banner01 .info p {
    color: #fff;
    margin: 0;
    margin-bottom: 5px;
}

.banner01 .info a {
    color: #fff;
    border: solid 1px #fff;
    padding: 1px 10px 2px 10px;
    text-decoration: none;
}

.banner01 .info a:hover {
	opacity: .7;
}

.banner01 img:hover{
	opacity: .7;
}

.banner01 .col-md-4{
	padding: 0 !important;
}

.myCarouselWrap {
	margin-bottom: 50px;
}

.subsc .es_caption  {
    float: left;
    width: 40%;
    font-size: 16px;
    padding-top: 15px;
}

.subsc .es_subscription_form_submit {
	float: right;
	margin-right: 50px;
    padding: 0px 20px !important;
    height: 40px !important;
    margin-top: 5px;
    color: #503c3c;
    font-size: 14px;
}


.subsc .ig_es_form_field_name, .subsc .ig_es_form_field_email{
    width: 200px;
    padding: 10px;
    margin-top: 5px;
    margin-left: 0px !important;
}

.subsc .es-field-wrap {
	float: left;
	width: 20%;
}

.es_subscription_form_submit{
	height: 52px !important;
}

.header-inner{
	padding: .5em !important;
}

.subsc {
    background: #f3e9e9;
    padding-top: 20px;
}

.subsc form {
	text-align: center;
}

.ig_es_form_field_name {
	margin-left: 10px !important;
}

.ig_es_form_field_email {
	margin-left: 50px !important;
}

.es_subscription_form_submit{
	background: #b5b527 !important;
}

.wrapPost .wrap {
    border-bottom: solid 3px gray;
    margin-bottom: 20px;
}

.wrapPost .wrap h2 {
    padding: 0px;
    margin: 0px;
    padding-top: 15px;
    color: #000000;
    text-decoration: unset;
    font-size: 30px;
}

.footerImg .col-md-2{
	margin: 0px !important;
	padding: 0px !important;
}

.footerImg .col-md-2 img{
	height: 180px;
	width: 100%;
}

#site-footer{
	background-color: #262e26 !important;
	padding-top: 0px !important;
	margin-top: 20px;
}

#site-footer .section-inner{
	padding-top: 30px;
}

#site-footer .footer-copyright {
	color: #727272 !important;
	font-weight: 400;
}

.carousel-control {
	width: 5%;
}

.multi-item-carousel .col-xs-4{
	padding: 0 !important;
}
.multi-item-carousel .col-xs-4:hover{
	opacity: .8;
}

.multi-item-carousel .carousel-inner > .item {
  transition: 500ms ease-in-out left;
}
.multi-item-carousel .carousel-inner .active.left {
  left: -33%;
}
.multi-item-carousel .carousel-inner .active.right {
  left: 33%;
}
.multi-item-carousel .carousel-inner .next {
  left: 33%;
}
.multi-item-carousel .carousel-inner .prev {
  left: -33%;
}
@media all and (transform-3d), (-webkit-transform-3d) {
  .multi-item-carousel .carousel-inner > .item {
    transition: 500ms ease-in-out left;
    transition: 500ms ease-in-out all;
    backface-visibility: visible;
    transform: none!important;
  }
}
.multi-item-carousel .carouse-control.left,
.multi-item-carousel .carouse-control.right {
  background-image: none;
}
body {
  background: #333;
  color: #ddd;
}
h1 {
  color: white;
  font-size: 2.25em;
  text-align: center;
  margin-top: 1em;
  margin-bottom: 2em;
  text-shadow: 0px 2px 0px #000000;
}
.carousel-inner>.item>a>img, .carousel-inner>.item>img, .img-responsive, .thumbnail a>img, .thumbnail>img {
    width: 100% !important;
}
.myCarouselWrap .title  {
    padding-left: 20px;
    position: absolute;
    bottom: 0;
}
.myCarouselWrap .title p, .myCarouselWrap .title a {
	color: #ffffff;
	text-decoration: none;
}
.myCarouselWrap .readmore {
    border: solid 2px #ffffff;
    padding: 2px 6px;
}
.container {
	width: 80% !important;
}
@media screen and (max-width: 500px){
		.carousel-inner>.item>a>img, .carousel-inner>.item>img, .img-responsive, .thumbnail a>img, .thumbnail>img {
		    height: 150px !important;
		    width: 100% !important;
		}
		.footerImg .col-md-2 img {
		    height: auto;
		    width: 100%;
		}
		svg, img, embed, object {
	    display: block;
	    height: auto;
	    max-width: 100%;
	    text-align: center;
	    width: 90%;
	    margin: 0 auto;
	}
	.pld-column, .pld-columns {
	    position: relative;
	    padding-left: 0.9375em;
	    padding-right: 0.9375em;
	    float: left;
	    margin: 20px;
	}
	.subsc .es-field-wrap {
	    float: unset;
	    width: 100%;
	}
	.subsc {
	    background: #f3e9e9;
	    padding-top: 20px;
	    padding-left: 50px;
	    padding-bottom: 20px;
	}
	.subsc .es_subscription_form_submit {
	    float: right;
	    margin-right: 87px;
	}
	.nav-toggle .toggle-inner {
	    display: none;
	}
	.header-titles {
	    width: 130px;
	    position: absolute;
	    right: 0;
	}
	.header-titles a img, .header-titles img{
		width:  auto;
		float: left;
	}
	.header-inner {
	    padding: 3rem 0 !important;
	}
}

</style>

		<?php wp_head(); ?>

	</head>

	<body <?php body_class(); ?>>

		<?php
		wp_body_open();
		?>

		<header id="site-header" class="header-footer-group" role="banner">
			<div class="header-inner section-inner">

				<div class="header-titles-wrapper">

					<?php

					// Check whether the header search is activated in the customizer.
					$enable_header_search = get_theme_mod( 'enable_header_search', true );

					if ( true === $enable_header_search ) {

						?>

						<button class="toggle search-toggle mobile-search-toggle" data-toggle-target=".search-modal" data-toggle-body-class="showing-search-modal" data-set-focus=".search-modal .search-field" aria-expanded="false">
							<span class="toggle-inner">
								<span class="toggle-icon">
									<?php twentytwenty_the_theme_svg( 'search' ); ?>
								</span>
								<span class="toggle-text"><?php _ex( 'Search', 'toggle text', 'twentytwenty' ); ?></span>
							</span>
						</button><!-- .search-toggle -->

					<?php } ?>

					<div class="header-titles">
<a href=''>						
<img src="<?php echo get_template_directory_uri(); ?>/assets/images/fb.png" width="" height="" alt="" /></a>
<a href=''>						
<img src="<?php echo get_template_directory_uri(); ?>/assets/images/in.png" width="" height="" alt="" /></a>
<img src="<?php echo get_template_directory_uri(); ?>/assets/images/tw.png" width="" height="" alt="" /></a>
<img src="<?php echo get_template_directory_uri(); ?>/assets/images/yt.png" width="" height="" alt="" /></a>
					</div><!-- .header-titles -->

					<button class="toggle nav-toggle mobile-nav-toggle" data-toggle-target=".menu-modal"  data-toggle-body-class="showing-menu-modal" aria-expanded="false" data-set-focus=".close-nav-toggle">
						<span class="toggle-inner">
							<span class="toggle-icon">
								<?php twentytwenty_the_theme_svg( 'ellipsis' ); ?>
							</span>
							<span class="toggle-text"><?php _e( 'Menu', 'twentytwenty' ); ?></span>
						</span>
					</button><!-- .nav-toggle -->

				</div><!-- .header-titles-wrapper -->

				<div class="header-navigation-wrapper">

					<?php
					if ( has_nav_menu( 'primary' ) || ! has_nav_menu( 'expanded' ) ) {
						?>

						 

						<?php
					}

					if ( true === $enable_header_search || has_nav_menu( 'expanded' ) ) {
						?>

						<div class="header-toggles hide-no-js">

						<?php
						if ( has_nav_menu( 'expanded' ) ) {
							?>

							<div class="toggle-wrapper nav-toggle-wrapper has-expanded-menu">

								<button class="toggle nav-toggle desktop-nav-toggle" data-toggle-target=".menu-modal" data-toggle-body-class="showing-menu-modal" aria-expanded="false" data-set-focus=".close-nav-toggle">
									<span class="toggle-inner">
										<span class="toggle-text"><?php _e( 'Menu', 'twentytwenty' ); ?></span>
										<span class="toggle-icon">
											<?php twentytwenty_the_theme_svg( 'ellipsis' ); ?>
										</span>
									</span>
								</button><!-- .nav-toggle -->

							</div><!-- .nav-toggle-wrapper -->

							<?php
						}

						if ( true === $enable_header_search ) {
							?>

							<div class="toggle-wrapper search-toggle-wrapper">

								<button class="toggle search-toggle desktop-search-toggle" data-toggle-target=".search-modal" data-toggle-body-class="showing-search-modal" data-set-focus=".search-modal .search-field" aria-expanded="false">
									<span class="toggle-inner">
										<?php twentytwenty_the_theme_svg( 'search' ); ?>
										<span class="toggle-text"><?php _ex( 'Search', 'toggle text', 'twentytwenty' ); ?></span>
									</span>
								</button><!-- .search-toggle -->

							</div>

							<?php
						}
						?>

						</div><!-- .header-toggles -->
						<?php
					}
					?>

				</div><!-- .header-navigation-wrapper -->

			</div><!-- .header-inner -->

			<?php
			// Output the search modal (if it is activated in the customizer).
			if ( true === $enable_header_search ) {
				get_template_part( 'template-parts/modal-search' );
			}
			?>

		</header><!-- #site-header -->
		<!-- #site-header -->


	<div class="row">
		<div class="container">
			<div class="logo21">
				<a href=''>						
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" width="" height="" alt="" class="center" /></a>
			</div>
	  	</div>
	</div>


		<?php
		// Output the menu modal.
		get_template_part( 'template-parts/modal-menu' );
