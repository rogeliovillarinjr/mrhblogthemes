<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>
			<footer id="site-footer" role="contentinfo" class="header-footer-group">
				<div class='row footerImg'>
						<div class='col-md-2' style='background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/pic09.jpg); background-size: cover; background-repeat: no-repeat;'>
							<img style='opacity:0;' src="<?php echo get_template_directory_uri(); ?>/assets/images/pic09.jpg" width="" height="" alt="" class="center" />
						</div>
						<div class='col-md-2' style='background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/pic10.jpg); background-size: cover; background-repeat: no-repeat;'>
							<img style='opacity:0;' src="<?php echo get_template_directory_uri(); ?>/assets/images/pic09.jpg" width="" height="" alt="" class="center" />
						</div>
						<div class='col-md-2' style='background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/pic11.jpg); background-size: cover; background-repeat: no-repeat;'>
							<img style='opacity:0;' src="<?php echo get_template_directory_uri(); ?>/assets/images/pic09.jpg" width="" height="" alt="" class="center" />
						</div>
						<div class='col-md-2' style='background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/pic12.jpg); background-size: cover; background-repeat: no-repeat;'>
							<img style='opacity:0;' src="<?php echo get_template_directory_uri(); ?>/assets/images/pic12.jpg" width="" height="" alt="" class="center" />
						</div>
						<div class='col-md-2' style='background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/pic13.jpg); background-size: cover; background-repeat: no-repeat;'>
							<img style='opacity:0;' src="<?php echo get_template_directory_uri(); ?>/assets/images/pic13.jpg" width="" height="" alt="" class="center" />
						</div>
						<div class='col-md-2' style='background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/pic14.jpg); background-size: cover; background-repeat: no-repeat;'>
							<img style='opacity:0;' src="<?php echo get_template_directory_uri(); ?>/assets/images/pic14.jpg" width="" height="" alt="" class="center" />
						</div>
				</div>
				<div class="section-inner">

					<div class="footer-credits">

						<p class="footer-copyright">&copy;
							<?php
							echo date_i18n(
								/* translators: Copyright date format, see https://www.php.net/manual/datetime.format.php */
								_x( 'Y', 'copyright date format', 'twentytwenty' )
							);
							?>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a>
						</p><!-- .footer-copyright -->

						<p class="powered-by-wordpress">
							<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'twentytwenty' ) ); ?>">
								<?php _e( 'Powered by WordPress', 'twentytwenty' ); ?>
							</a>
						</p><!-- .powered-by-wordpress -->

					</div><!-- .footer-credits -->

					<a class="to-the-top" href="#site-header">
						<span class="to-the-top-long">
							<?php
							/* translators: %s: HTML character for up arrow. */
							printf( __( 'To the top %s', 'twentytwenty' ), '<span class="arrow" aria-hidden="true">&uarr;</span>' );
							?>
						</span><!-- .to-the-top-long -->
						<span class="to-the-top-short">
							<?php
							/* translators: %s: HTML character for up arrow. */
							printf( __( 'Up %s', 'twentytwenty' ), '<span class="arrow" aria-hidden="true">&uarr;</span>' );
							?>
						</span><!-- .to-the-top-short -->
					</a><!-- .to-the-top -->

				</div><!-- .section-inner -->

			</footer><!-- #site-footer -->

		<?php wp_footer(); ?>

	</body>
</html>
