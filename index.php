<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
?>

<main id="site-content1" role="main">
	<div class="myCarouselWrap">

  <div class="row">
    <div class="col-md-12">
      <div class="carousel slide multi-item-carousel" id="theCarousel">
		 <div class="carousel-inner">
			<?php
				$args = array(
				'post_type'=> 'post',
				'orderby'    => 'ID',
				'post_status' => 'publish',
				'order'    => 'DESC',
				'posts_per_page' => -1 // this will retrive all the post that is published 
				);
				$result = new WP_Query( $args );
				if ( $result-> have_posts() ) : 
			?>
			<?php while ( $result->have_posts() ) : $result->the_post(); ?>
			<?php
				$feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID)); 
				$post_idx = $post->ID[0];
				$idpost = get_the_ID();
				if($idpost == 1){
					$actPost = "active";
				}else{
					$actPost = "";
				}
			?>
          <div class="item <?php echo $actPost; ?>">
            <div class="col-xs-4" style='background-image: url(<?php echo $feat_image; ?>); background-size: cover; background-repeat: no-repeat; height: 350px;'>
            	<a href="<?php echo get_permalink($post->ID); ?>">
            		<img style='opacity: 0;' src="<?php echo $feat_image; ?>" class="img-responsive">
            		<div class="title">
	            		<p class='titlep'>
	            			<?php echo the_title(); ?> <br />
	            			<a class='readmore'>Read More</a>
	            		</p>
            		</div>
            	</a>
            </div>
          </div>

 		<?php endwhile; ?>
		<?php endif; wp_reset_postdata(); ?>
          
          <!--  Example item end -->
        </div>


        <a class="left carousel-control" href="#theCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
        <a class="right carousel-control" href="#theCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
      </div>
    </div>
  </div>

<script>
	// Instantiate the Bootstrap carousel
$('.multi-item-carousel').carousel({
  interval: false
});

// for every slide in carousel, copy the next slide's item in the slide.
// Do the same for the next, next item.
$('.multi-item-carousel .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  if (next.next().length>0) {
    next.next().children(':first-child').clone().appendTo($(this));
  } else {
  	$(this).siblings(':first').children(':first-child').clone().appendTo($(this));
  }
});
</script>
 
 
	</div>
	<div class="row">
		<div class="container subsc">
			<?php echo do_shortcode('[email-subscribers-form id="1"]'); ?>
		</div>
	</div><br /><br />

	<div class="row wrapPost">
		<div class="container">
		  <div class="col-md-9">
		  	<div class="row">
		  		<?php echo do_shortcode('[pld_post_list limit="20"]'); ?>
		  	</div>
		  </div>
		  <div class="col-md-3">
			<div class='row'>
					<a href=''>						
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/pic07.jpg" width="" height="" alt="" /></a>
			</div><br />
			<div class='row'>
					<a href=''>						
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/pic08.jpg" width="" height="" alt="" /></a>
			</div>
		  </div>
	  	</div>
	</div>
</main><!-- #site-content -->

 
<?php
get_footer();
